# Pocketbook External Packages (aka. ports)

**You can find prebuilt packages int the Artifacts**

1. Write your own `.pocketbuild.env` for your package
2. Install appropriate Pocketbook SDK on your host system
3. Run `PBOOK_SDK_PATH=... ./build.sh <your-pocketbuild.env> pbook-sdk`
4. This script will download source code, unpack it, compile and then make optional `.tar.gz` package
5. To install built package, just copy its `build/mnt/ext1/` contents onto your reader `system` directory while syncing
