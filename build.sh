#!/usr/bin/env bash

set -e

# Source SDK config

SDK=$2 . ./build.env

# Get RECIPE file name from cmdline

RECIPE=$1

# Check if RECIPE file exists

if [ ! -f "${RECIPE}" ]; then echo "ERROR: Recipe file not found"; exit 1; fi

# Source RECIPE file contents

. ${RECIPE}

# Remember build script root directory

ROOT_DIR=${PWD}

# Check RECIPE/tarball and RECIPE/package values

if [ "${tarball}" == "" ]; then echo "ERROR: tarball is missing in ${RECIPE}"; exit 1; fi

if [ "${tarball}" == "" ]; then echo "ERROR: package is missing in ${RECIPE}"; exit 1; fi

echo "INFO: Assigning and creating directories"

# Assign POCKETBUILD_DIR, SOURCE_DIR and BUILD_DIR

STARTING_FROM_DIR="${PWD}"
POCKETBUILD_DIR="${PWD}/$(echo ${RECIPE} | sed -e 's/.pocketbuild.env//g')"
SOURCE_DIR="${POCKETBUILD_DIR}/source"
BUILD_DIR="${POCKETBUILD_DIR}/build"

# Assign SOURCE_REMOTE, SOURCE_LOCAL and SOURCE_SUBDIR

SOURCE_REMOTE="${tarball}"
SOURCE_LOCAL="${POCKETBUILD_DIR}/${tarball##*/}"
SOURCE_SUBDIR="${SOURCE_DIR}/${package}"

# Prepare SOURCE_DIR and BUILD_DIR

mkdir -p ${SOURCE_DIR} ${BUILD_DIR}

# Assign DESTDIR

DESTDIR="${BUILD_DIR}" 

# Prepare DESTDIR

mkdir -p ${DESTDIR}

# Fetch SOURCE_REMOTE to SOURCE_LOCAL

echo "INFO: Fetching source ..."

if [ ! -f "${SOURCE_LOCAL}" ]; then
    wget ${SOURCE_REMOTE} -O ${SOURCE_LOCAL}
else
    echo "INFO: SOURCE_LOCAL already exists, do not reload it"
fi

# Unpack SOURCE_LOCAL to SOURCE_DIR

echo "INFO: Unpacking source ..."

if [ ! $(ls -A ${SOURCE_DIR}) ]; then
    tar xf ${SOURCE_LOCAL} -C ${SOURCE_DIR}
else
    echo "INFO: SOURCE_DIR already contains source code, do not unpack SOURCE_LOCAL to it"
fi

# Change to the SOURCE_SUBDIR

echo "INFO: Changing to source sub directory ..."

cd ${SOURCE_SUBDIR}

# Copy all the listed package local files from env

for PKG_LOCAL_FILE in ${PKG_LOCAL_FILES}; do
    echo "Copying local file: ${PKG_LOCAL_FILE}"
    cp ${ROOT_DIR}/${PKG_LOCAL_FILE} .
done

# Run prepare() function from env

echo "INFO: Running prepare() ..."

prepare || ( echo "ERROR: prepare() function failed. Exiting with error code: 1"; exit 1 )

# Run build() function from env

echo "INFO: Running build() ..."

build || ( echo "ERROR: build() function failed. Exiting with error code: 1"; exit 1 )

echo "INFO: Creating complete package ..."

# Change to the BUILD_DIR

cd ${BUILD_DIR}

tar zcf ../$(echo ${RECIPE} | sed -e 's/.pocketbuild.env//g').tar.gz .

# Print the final message to the user, then exit with no error code (0)

echo "INFO: You should be able to locate your built package in the '${BUILD_DIR}'"; exit 0
